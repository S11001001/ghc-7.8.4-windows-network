Windows network built bug
-------------------------

[Originally found in Ermine.](https://github.com/ermine-language/ermine/issues/17)

Reproduce like so:

```sh
git clone https://bitbucket.org/S11001001/ghc-7.8.4-windows-network.git
cd ghc-7.8.4-windows-network
cabal sandbox init
cabal install --only-dependencies --enable-tests
cabal configure
cabal build
```

Should print:

```
>cabal build
Building ghc784-windows-network-0.1.0.0...
Preprocessing library ghc784-windows-network-0.1.0.0...
[1 of 2] Compiling NetTest          ( src\NetTest.hs, dist\build\NetTest.o )
[2 of 2] Compiling NetTest2         ( src\NetTest2.hs, dist\build\NetTest2.o )
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
Loading package array-0.5.0.0 ... linking ... done.
Loading package deepseq-1.3.0.2 ... linking ... done.
Loading package containers-0.5.5.1 ... linking ... done.
Loading package pretty-1.1.1.1 ... linking ... done.
Loading package template-haskell ... linking ... done.
Loading package bytestring-0.10.4.0 ... linking ... done.
Loading package network-2.6.0.2 ... linking ... 
<no location info>:
    ghc.exe: unable to load package `network-2.6.0.2'
ghc.exe: J:\scompall\src\ghc-7.8.4-windows-network\.cabal-sandbox\x86_64-windows-ghc-7.8.3\network-2.6.0.2\HSnetwork-2.6.0.2.o: unknown symbol `shutdownWinSock'
```

Without Template Haskell
------------------------

Use branch `without-th` to reproduce without Template Haskell.  Just
run `cabal repl` instead of `cabal build`.
